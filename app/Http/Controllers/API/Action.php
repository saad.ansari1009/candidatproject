<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Action extends Controller
{
    public function AffectationPseudo(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'pseudo' => $req->Pseudo,
                        ]);
                    return "Candidat Pseudo successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function AffectationFormation(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {
                    DB::table('formationcandidat')->insert([
                        'candidat' => $id,
                        'formation' => $req->Formation,
                    ]);
                    $ObjectF = DB::table('formation')->where('id', $req->Formation)->get();
                    $typeF = $ObjectF[0]->formationType;
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'validation' => $typeF,
                        ]);
                    return "Candidat Validation / Formation successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function listPseudo(Request $req)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {

                    $data = DB::select('select * from pseudo');
                    return [
                        "data" => $data,
                        "Pseudos successfully imported"
                    ];
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getFormationList()
    {
        try {
            $test = DB::select('SELECT
            (select count(c.formation) from formationcandidat c
            where c.formation=f.id
            ) as "NBR candidats",
            f.id as ID ,
            f.type as "Libelle",
            f.DateDebut as "Date Debut",
            f.DateFin "Date Fin"
            from formation f
            ');
            return $test;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
