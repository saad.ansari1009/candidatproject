<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get("/login",'API\Connexion@login');
Route::get("/logout",'API\Connexion@logout');

// Formation routes 
Route::get("/addFormation",'API\Formation@addFormation');
Route::get("/getFormation",'API\Formation@getFormation');
Route::get("/updateFormation/{id}",'API\Formation@updateFormation');
Route::get("/addTypeFormation",'API\Formation@addTypeFormation');

// Absences routes
Route::get("/getAbsenceType/{id}",'API\Absence@getAbsenceType'); // Absence by id Formation
Route::get("/listAbsence",'API\Absence@listAbsence'); // All absences


// Candidats routes
Route::get("/getCandidat",'API\Candidat@getCandidat');
Route::get("/getNewCandidat", 'API\Candidat@getNewCandidat');
Route::get("/getCandidatId/{id}", 'API\Candidat@getCandidatId');
Route::get("/updateCandidat/{id}",'API\Candidat@updateCandidat');
Route::get("/updateCandidatValidation/{id}", 'API\Candidat@updateCandidatValidation');


// Actions routes
Route::get("/listPseudo",'API\Action@listPseudo');
Route::get("/AffectationPseudo/{id}",'API\Action@AffectationPseudo');
Route::get("/AffectationFormation/{id}",'API\Action@AffectationFormation');
Route::get("/getFormationList", 'API\Action@getFormationList');

